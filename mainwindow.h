#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "parabolic.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(ParabolicEquation &eq,QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_calc_butt_clicked();

    void on_prev_butt_clicked();

    void on_next_butt_clicked();

private:
    Ui::MainWindow *ui;

    ParabolicEquation eq;
    Solver *solver;

    QString last_method;

    int cur_t_step;
    int cur_t_steps;
    int cur_x_steps;
    double cur_max_t;

    void plot_original_solution(double h, int x_steps, double t);
    void plot_numeric_solution(int t_step);
    void plot_error(double h, int x_steps, double tau, int t_step);
};
#endif // MAINWINDOW_H
