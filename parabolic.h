#pragma once

#include <iostream>
#include <functional>
#include <vector>


#include "tridiagonal.h"


using NumericSolution = std::vector<std::vector<double>>;

class ParabolicEquation
{
public:
    double a, b, c;

    std::function<double(double, double)> f;
    std::function<double(double)> phi;

    double alpha1;
    double alpha2;

    double beta1;
    double beta2;

    std::function<double(double)> gamma1;
    std::function<double(double)> gamma2;

    std::function<double(double, double)> solution;

    double length;

    ParabolicEquation(double a, double b, double c, std::function<double(double, double)> f, std::function<double(double)> phi,
                      double alpha1, double beta1, std::function<double(double)> gamma1,
                      double alpha2, double beta2, std::function<double(double)> gamma2, 
                      double length, std::function<double(double, double)> solution
                     ) 
    {
        this->a = a;
        this->b = b;
        this->c = c;
        this->f = f;

        this->phi = phi;
        
        this->alpha1 = alpha1;
        this->beta1 = beta1;
        this->gamma1 = gamma1;
         
        this->alpha2 = alpha2;
        this->beta2 = beta2;
        this->gamma2 = gamma2;

        this->length = length;

        this->solution = solution;
    }
};

class Solver
{
public:
    const static int TWO_POINTS_FIRST_ORDER = 0;
    const static int TWO_POINTS_SECOND_ORDER = 1;
    const static int THREE_POINTS_SECOND_ORDER = 2;

    virtual void Solve(const ParabolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation) = 0;
    
    const NumericSolution& GetSolution() 
    {
        return u;
    }

    virtual ~Solver() {}
protected:
    NumericSolution u;
};

class ExplicitScheme: public Solver
{
public:
    void Solve(const ParabolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation) override;
};


class ImplicitScheme: public Solver
{
public:
    void Solve(const ParabolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation) override;
};



class CrankNicolsonScheme: public Solver
{
public:
    void Solve(const ParabolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation) override;
};

