#include "parabolic.h"


void ExplicitScheme::Solve(const ParabolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation)
{
    u = NumericSolution(t_steps + 1, std::vector<double> (x_steps + 1));

    double h = eq.length / x_steps;
    double tau = t_max / t_steps;

    for (int j = 0; j <= x_steps; ++j)
    {
        u[0][j] = eq.phi(j * h);
    }

    for (int i = 0; i < t_steps; ++i)
    {
        for (int j = 1; j < x_steps; ++j)
        {
            double sig = eq.a * tau / (h * h);
            double coef1 = (sig - eq.b * tau / (2 * h));
            double coef2 = (1 - 2 * sig + eq.c * tau);
            double coef3 = (sig + eq.b * tau / (2 * h));

            u[i + 1][j] = coef1 * u[i][j - 1] + coef2 * u[i][j] + coef3 * u[i][j + 1] + tau * eq.f(j * h, i * tau);
        }

        if (boundary_approximation == Solver::TWO_POINTS_FIRST_ORDER)
        {
            u[i + 1][0] = -(-eq.alpha1 * u[i + 1][1] + h * eq.gamma1((i + 1) * tau)) / (eq.alpha1 - eq.beta1 * h);
            u[i + 1][x_steps] = (eq.alpha2 * u[i + 1][x_steps - 1] + h * eq.gamma2((i + 1) * tau)) / (eq.alpha2 + eq.beta2 * h);
        }
        else if (boundary_approximation == Solver::TWO_POINTS_SECOND_ORDER)
        {
            double k11 = (-eq.alpha1*(-eq.c*h*h*tau + 2*eq.a*tau + h*h)/(tau*h*(-eq.b*h + 2*eq.a)) + eq.beta1);
            double k12 = 2*eq.alpha1*eq.a/(h*(-eq.b*h + 2*eq.a));
            double k13 = eq.alpha1*(-h*h*tau*eq.f(0,i*tau) - h*h*u[i][0])/(tau*h*(-eq.b*h + 2*eq.a)) + eq.gamma1((i + 1)*tau);

            u[i + 1][0] = k12 * u[i + 1][1] / k11 + k13 / k11;

            double k21 = 2*eq.alpha2*eq.a/(h*(eq.b*h + 2*eq.a));
            double k22 = (eq.alpha2*(-eq.c*h*h*tau + 2*eq.a*tau + h*h)/(tau*h*(eq.b*h + 2*eq.a)) + eq.beta2);
            double k23 = eq.gamma2((i + 1) * tau) - eq.alpha2*(-h*h*tau*eq.f(eq.length, i*tau) - h*h*u[i][x_steps])/(tau*h*(eq.b*h + 2*eq.a));

            u[i + 1][x_steps] = k21*u[i + 1][x_steps - 1]/k22 + k23/k22;
        }
        else if (boundary_approximation == Solver::THREE_POINTS_SECOND_ORDER )
        {
            double k00 = -eq.alpha1*(2*eq.a*tau + h*h)/(tau*h*(-eq.b*h + 2*eq.a)) + eq.beta1;
            double k01 = 2*eq.alpha1*eq.a/(h*(-eq.b*h + 2*eq.a));
            double k02 = eq.alpha1*(-eq.c*h*h*tau*u[i][0] - h*h*tau*eq.f(0,(i + 1)*tau) - h*h*u[i][0])/(tau*h*(-eq.b*h + 2*eq.a)) + eq.gamma1((i + 1)*tau);

            u[i + 1][0] = -k01 * u[i + 1][1] / k00 + k02 / k00;

            double k10 = -2*eq.alpha2*eq.a/(h*(eq.b*h + 2*eq.a));
            double k11 = (eq.alpha2*(2*eq.a*tau + h*h)/(tau*h*(eq.b*h + 2*eq.a)) + eq.beta2);
            double k12 = eq.gamma2((i + 1) * tau) - eq.alpha2*(-eq.c*h*h*tau*u[i][x_steps] - h*h*tau*eq.f(eq.length, (i + 1)*tau) - h*h*u[i][x_steps])/(tau*h*(eq.b*h + 2*eq.a));

            u[i + 1][x_steps] = -k10 * u[i + 1][x_steps - 1]/ k11 + k12 / k11;
        }
    }
}


void ImplicitScheme::Solve(const ParabolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation)
{
    u = NumericSolution(t_steps + 1, std::vector<double> (x_steps + 1));

    double h = eq.length / (x_steps);
    double tau = t_max / (t_steps);

    for (int j = 0; j <= x_steps; ++j)
    {
        u[0][j] = eq.phi(j * h);
    }

    VecDouble lower(x_steps + 1, (-eq.a / (h * h) + eq.b / (2 * h)));
    VecDouble upper(x_steps + 1, (-eq.a / (h * h) - eq.b / (2 * h)));
    VecDouble diag(x_steps + 1, (1/tau + 2 * eq.a / (h * h) - eq.c));
    VecDouble rhs(x_steps + 1);

    for (int i = 0; i < t_steps; ++i)
    {
        for (int j = 1; j < x_steps; ++j)
        {
            rhs[j] = u[i][j] / tau + eq.f(j * h, (i + 1) * tau);
        }

        if (boundary_approximation == Solver::TWO_POINTS_FIRST_ORDER)
        {
            diag[0] = eq.beta1 * h - eq.alpha1;
            upper[0] = eq.alpha1;
            rhs[0] = h * eq.gamma1((i + 1) * tau);

            lower[x_steps] = -eq.alpha2;
            diag[x_steps] = eq.alpha2 + eq.beta2 * h;
            rhs[x_steps] = h * eq.gamma2((i + 1) * tau);
        }
        else if (boundary_approximation == Solver::TWO_POINTS_SECOND_ORDER)
        {
            diag[0] = -eq.alpha1*(2*eq.a*tau + h*h)/(tau*h*(-eq.b*h + 2*eq.a)) + eq.beta1;
            upper[0] = 2*eq.alpha1*eq.a/(h*(-eq.b*h + 2*eq.a));
            rhs[0] = eq.alpha1*(-eq.c*h*h*tau*u[i][0] - h*h*tau*eq.f(0,(i + 1)*tau) - h*h*u[i][0])/(tau*h*(-eq.b*h + 2*eq.a)) + eq.gamma1((i + 1)*tau);

            lower[x_steps] = -2*eq.alpha2*eq.a/(h*(eq.b*h + 2*eq.a));
            diag[x_steps] = (eq.alpha2*(2*eq.a*tau + h*h)/(tau*h*(eq.b*h + 2*eq.a)) + eq.beta2);
            rhs[x_steps] = eq.gamma2((i + 1) * tau) - eq.alpha2*(-eq.c*h*h*tau*u[i][x_steps] - h*h*tau*eq.f(eq.length, (i + 1)*tau) - h*h*u[i][x_steps])/(tau*h*(eq.b*h + 2*eq.a));
        }
        else if (boundary_approximation == Solver::THREE_POINTS_SECOND_ORDER)
        {
            double k00 = -(3*eq.alpha1)/(2*h) + eq.beta1;
            double k01 = 2*eq.alpha1/h;
            double k02 = -eq.alpha1/(2*h);

            diag[0] = k00 - lower[1] * k02 / upper[1];
            upper[0] = k01 - diag[1] * k02 / upper[1];
            rhs[0] = eq.gamma1((i+1) * tau) - rhs[1] * k02 / upper[1];

            double kn0 = eq.alpha2/(2*h);
            double kn1 = -2*eq.alpha2/h;
            double kn2 = (3*eq.alpha2)/(2*h) + eq.beta2;

            lower[x_steps] = kn1 - kn0 * diag[x_steps - 1] / lower[x_steps - 1];
            diag[x_steps] = kn2 - kn0 * upper[x_steps - 1] / lower[x_steps - 1];
            rhs[x_steps] = eq.gamma2((i + 1) * tau) - kn0 * rhs[x_steps - 1] / lower[x_steps - 1];
        }

        TridiagonalAlgorithm(lower, diag, upper, rhs, u[i + 1], x_steps + 1);
    }
}

void CrankNicolsonScheme::Solve(const ParabolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation)
{
    u = NumericSolution(t_steps + 1, std::vector<double> (x_steps + 1));

    double h = eq.length / (x_steps);
    double tau = t_max / (t_steps);

    for (int j = 0; j <= x_steps; ++j)
    {
        u[0][j] = eq.phi(j * h);
    }

    VecDouble lower(x_steps + 1, -eq.a/(2*h*h) + eq.b/(4*h));
    VecDouble diag(x_steps + 1, (1/tau + eq.a/(h*h) - eq.c/2));
    VecDouble upper(x_steps + 1, -eq.a/(2*h*h) - eq.b/(4*h));
    VecDouble rhs(x_steps + 1);

    for (int i = 0; i < t_steps; ++i)
    {
        for (int j = 1; j < x_steps; ++j)
        {
            rhs[j] = +u[i][j]/tau + eq.a*(u[i][j + 1] - 2*u[i][j] + u[i][j - 1])/(2*h*h) +
                      eq.b*(u[i][j + 1] - u[i][j - 1])/(4*h) + eq.c*u[i][j]/2 + eq.f(j*h, i*tau)/2 + eq.f(j*h,(i+1)*tau)/2;
        }

        if (boundary_approximation == Solver::TWO_POINTS_FIRST_ORDER)
        {
            diag[0] = eq.beta1 * h - eq.alpha1;
            upper[0] = eq.alpha1;
            rhs[0] = h * eq.gamma1((i + 1) * tau);

            lower[x_steps] = -eq.alpha2;
            diag[x_steps] = eq.alpha2 + eq.beta2 * h;
            rhs[x_steps] = h * eq.gamma2((i + 1) * tau);
        }
        else if (boundary_approximation == Solver::TWO_POINTS_SECOND_ORDER)
        {
            diag[0] = -eq.alpha1*(2*eq.a*tau + h*h)/(tau*h*(-eq.b*h + 2*eq.a)) + eq.beta1;
            upper[0] = 2*eq.alpha1*eq.a/(h*(-eq.b*h + 2*eq.a));
            rhs[0] = eq.alpha1*(-eq.c*h*h*tau*u[i][0] - h*h*tau*eq.f(0,(i + 1)*tau) - h*h*u[i][0])/(tau*h*(-eq.b*h + 2*eq.a)) + eq.gamma1((i + 1)*tau);

            lower[x_steps] = -2*eq.alpha2*eq.a/(h*(eq.b*h + 2*eq.a));
            diag[x_steps] = (eq.alpha2*(2*eq.a*tau + h*h)/(tau*h*(eq.b*h + 2*eq.a)) + eq.beta2);
            rhs[x_steps] = eq.gamma2((i + 1) * tau) - eq.alpha2*(-eq.c*h*h*tau*u[i][x_steps] - h*h*tau*eq.f(eq.length, (i + 1)*tau) - h*h*u[i][x_steps])/(tau*h*(eq.b*h + 2*eq.a));
        }
        else if (boundary_approximation == Solver::THREE_POINTS_SECOND_ORDER)
        {
            double k00 = -(3*eq.alpha1)/(2*h) + eq.beta1;
            double k01 = 2*eq.alpha1/h;
            double k02 = -eq.alpha1/(2*h);

            diag[0] = k00 - lower[1] * k02 / upper[1];
            upper[0] = k01 - diag[1] * k02 / upper[1];
            rhs[0] = eq.gamma1((i+1) * tau) - rhs[1] * k02 / upper[1];

            double kn0 = eq.alpha2/(2*h);
            double kn1 = -2*eq.alpha2/h;
            double kn2 = (3*eq.alpha2)/(2*h) + eq.beta2;

            lower[x_steps] = kn1 - kn0 * diag[x_steps - 1] / lower[x_steps - 1];
            diag[x_steps] = kn2 - kn0 * upper[x_steps - 1] / lower[x_steps - 1];
            rhs[x_steps] = eq.gamma2((i + 1) * tau) - kn0 * rhs[x_steps - 1] / lower[x_steps - 1];
        }

        TridiagonalAlgorithm(lower, diag, upper, rhs, u[i + 1], x_steps + 1);
    }
}
