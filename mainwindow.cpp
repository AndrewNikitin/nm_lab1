#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPixmap>

#include "qcustomplot/qcustomplot.h"

MainWindow::MainWindow(ParabolicEquation &eq, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , eq(eq)
{
    ui->setupUi(this);

//    ui->solution_plot->plotLayout()->insertRow(0);
//    ui->solution_plot->plotLayout()->addElement(0, 0, new QCPTextElement(ui->solution_plot, "Решение при t = " + QString::number(0.0), QFont("sans", 12, QFont::Bold)));

    ui->solution_plot->addGraph(ui->solution_plot->xAxis, ui->solution_plot->yAxis);
    ui->solution_plot->addGraph(ui->solution_plot->xAxis, ui->solution_plot->yAxis);

    ui->solution_plot->xAxis->setLabel("x");
    ui->solution_plot->yAxis->setLabel("U(x, t)");

//    ui->error_plot->plotLayout()->insertRow(0);
//    ui->error_plot->plotLayout()->addElement(0, 0, new QCPTextElement(ui->error_plot, "Ошибка при t = " + QString::number(0.0), QFont("sans", 12, QFont::Bold)));

    ui->error_plot->addGraph(ui->error_plot->xAxis, ui->error_plot->yAxis);

    ui->error_plot->xAxis->setLabel("x");
    ui->error_plot->xAxis->setRange(0.0, eq.length);

    ui->error_plot->yAxis->setLabel("Error(x)");
    ui->error_plot->yAxis->setRange(0.0, 1.0);

// plot_original_solution(0.01, 200, 0.0);

    last_method = "";
    solver = nullptr;

    cur_t_step = -1;
    cur_max_t = 0.0;
    cur_t_steps = 0;
    cur_x_steps = 0;

    this->setFixedSize(1200,750);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::plot_numeric_solution(int t_step)
{
    int x_steps = solver->GetSolution()[t_step].size();
    QVector<double> x(x_steps);
    QVector<double> u(x_steps);

    for (int i = 0; i < x_steps; ++i)
    {
        x[i] = i * (eq.length / (x_steps - 1));
        u[i] = solver->GetSolution()[t_step][i];
        std::cout << u[i] << ' ';
    }
    std::cout << std::endl;
    ui->solution_plot->graph(1)->setData(x, u);
    ui->solution_plot->rescaleAxes();
}

void MainWindow::plot_original_solution(double h, int x_steps, double t)
{
    QVector<double> x(x_steps + 1);
    QVector<double> u(x_steps + 1);

    for (int i = 0; i <= x_steps; ++i)
    {
        x[i] = i * h;
        u[i] = eq.solution(i*h, t);
        std::cout << u[i] << ' ';
    }
    std::cout << std::endl;
    ui->solution_plot->graph(0)->setData(x, u);
    ui->solution_plot->rescaleAxes();
}

void MainWindow::plot_error(double h, int x_steps, double tau, int t_step)
{
    QVector<double> x(x_steps + 1);
    QVector<double> error(x_steps + 1);

    std::cout << h << ' ' << tau << std::endl;

    for (int i = 0; i <= x_steps; ++i)
    {
        x[i] = i * h;
        error[i] = std::abs(solver->GetSolution()[t_step][i] - eq.solution(i*h, tau * t_step));
    }

    ui->error_plot->graph(0)->setData(x, error);
    ui->error_plot->rescaleAxes();
}

void MainWindow::on_calc_butt_clicked()
{
    int x_steps = ui->steps_x_spinbox->value();
    int t_steps = ui->steps_t_spinbox->value();

    double x_max = ui->x_max_spinbox->value();
    double t_max = ui->t_max_spinbox->value();


    QString method = ui->methods_box->currentText();
    if ( last_method != method )
    {
        if (solver != nullptr)
            delete solver;
        if ( method == "Явная схема" )
            solver = new ExplicitScheme;
        else if ( method == "Неявная схема" )
            solver = new ImplicitScheme;
        else if ( method == "Схема Кранка-Николсона" )
            solver = new CrankNicolsonScheme;
    }

    int approximation_type = 0;
    if ( ui->approximation_box->currentText() == "По 2 точкам первого порядка" )
        approximation_type = Solver::TWO_POINTS_FIRST_ORDER;
    else if ( ui->approximation_box->currentText() == "По 2 точкам второго порядка" )
        approximation_type = Solver::TWO_POINTS_SECOND_ORDER;
    else if ( ui->approximation_box->currentText() == "По 3 точкам второго порядка")
        approximation_type = Solver::THREE_POINTS_SECOND_ORDER;

    solver->Solve(eq, t_steps, t_max, x_steps, approximation_type);

    cur_t_step = 0;
    cur_t_steps = t_steps;
    cur_x_steps =x_steps;
    cur_max_t = t_max;

    double h = eq.length / x_steps;
    double tau = t_max / t_steps;

    if (method == "Явная схема" && cur_x_steps > int(std::sqrt(0.5 / (eq.a * tau)) * eq.length))
    {
        cur_x_steps = int(std::sqrt(0.5 / (eq.a * tau)) * eq.length);
        ui->steps_x_spinbox->setValue(cur_x_steps);
    }

    plot_numeric_solution(0);
    plot_original_solution(h, cur_x_steps, 0);
    plot_error(h, cur_x_steps, tau, 0);

    ui->solution_plot->replot();
    ui->error_plot->replot();

//    std::cout << h << ' ' << x_steps << ' ' << t_steps * tau << std::endl;

    for (int j = 0; j < solver->GetSolution()[0].size(); ++j)
         std::cout << std::abs(solver->GetSolution()[0][j] - eq.solution(j * h, 0)) << ' ';
    std::cout << std::endl;
    std::cout << "-???" << std::endl;
    last_method = method;
}

void MainWindow::on_prev_butt_clicked()
{
    if ( cur_t_step == -1 )
        return;
    std::cout << cur_t_step << std::endl;
    cur_t_step -= 1;
    if ( cur_t_step == -1)
    {
        cur_t_step = cur_t_steps;
    }
    double h = eq.length / cur_x_steps;
    double tau = cur_max_t / cur_t_steps;

    plot_numeric_solution(cur_t_step);
    plot_original_solution(h, cur_x_steps, cur_t_step * tau);
    plot_error(h, cur_x_steps, tau, cur_t_step);

    ui->solution_plot->replot();
    ui->error_plot->replot();
}

void MainWindow::on_next_butt_clicked()
{
    if ( cur_t_step == -1 )
        return;
    std::cout << cur_t_step << std::endl;
    cur_t_step += 1;
    if ( cur_t_step == cur_t_steps + 1)
    {
        cur_t_step = 0;
    }
    double h = eq.length / cur_x_steps;
    double tau = cur_max_t / cur_t_steps;

    plot_numeric_solution(cur_t_step);
    plot_original_solution(h, cur_x_steps, cur_t_step * tau);
    plot_error(h, cur_x_steps, tau, cur_t_step);

    ui->solution_plot->replot();
    ui->error_plot->replot();
}
