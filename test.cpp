#include <iostream>
#include <cmath>
#include "parabolic.h"

int main(int argc, char const *argv[])
{
    ParabolicEquation eq(
            1, 0, -1,
            [](double x, double t) { return 0; },
            [](double x) { return sin(x); },
            1.0, 0.0, [](double t) { return exp(-2*t); },
            0.0, 1.0, [](double t) { return exp(-2*t); },
            M_PI / 2, [](double x, double t) { return exp(-2 * t) * sin(x); }
        );

    double T = 1.0;

    int t_steps;
    int x_steps;

    std::cin >> x_steps >> t_steps;

    double tau = T / t_steps;

    // if (x_steps > int(std::sqrt(0.5 / (eq.a * tau)) * eq.length))
    //     x_steps = int(std::sqrt(0.5 / (eq.a * tau)) * eq.length);

    double h = eq.length / x_steps;

    std::cout << "h = " << h << ", tau =" << tau << std::endl;

    CrankNicolsonScheme solver;
    
    solver.Solve(eq, t_steps, T, x_steps, Solver::TWO_POINTS_FIRST_ORDER);
    double max_error = 0.0;
    for (int i = 0; i <= x_steps; ++i)
    {
        max_error = std::max(max_error, std::abs(eq.solution(i * h, T) - solver.GetSolution()[t_steps][i]));
    }
    std::cout << "2:1 " << max_error << '\n';

    solver.Solve(eq, t_steps, T, x_steps, Solver::THREE_POINTS_SECOND_ORDER);
    max_error = 0.0;
    for (int i = 0; i <= x_steps; ++i)
    {
        max_error = std::max(max_error, std::abs(eq.solution(i * h, T) - solver.GetSolution()[t_steps][i]));
    }
    std::cout << "3:2 " << max_error << '\n';


    solver.Solve(eq, t_steps, T, x_steps, Solver::TWO_POINTS_SECOND_ORDER);
    max_error = 0.0;
    for (int i = 0; i <= x_steps; ++i)
    {
        max_error = std::max(max_error, std::abs(eq.solution(i * h, T) - solver.GetSolution()[t_steps][i]));
    }
    std::cout << "2:2 " << max_error << '\n';
    return 0;
}