#include "tridiagonal.h"

void TridiagonalAlgorithm(VecDouble &lower, VecDouble &diag, VecDouble &upper, VecDouble &rhs, VecDouble &ans, int n)
{
    VecDouble alpha(n + 1, 0);
    VecDouble beta(n + 1, 0);

    alpha[1] = -upper[0] / diag[0];
    beta[1] = rhs[0] / diag[0];

    for (int i = 0; i <= n - 1; ++i)
    {
        double denominator = lower[i] * alpha[i] + diag[i];
        alpha[i + 1] = -upper[i] / denominator;
        beta[i + 1] = (rhs[i] - lower[i] * beta[i]) / denominator;
    }

    ans[n - 1] = beta[n];
    for (int i = n - 2; i >= 0; --i)
    {
        ans[i] = alpha[i + 1] * ans[i + 1] + beta[i + 1];
    }
}
