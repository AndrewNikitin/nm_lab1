#include <iostream>
#include <cmath>

#include <QApplication>

#include "parabolic.h"
#include "mainwindow.h"

using std::sin;
using std::cos;
using std::exp;


int main(int argc, char *argv[])
{
    ParabolicEquation eq(
            1, 2, 1,
            [](double x, double t) { return exp(-x) * cos(t + M_PI/4); },
            [](double x) { return exp(-x) * sin(M_PI_4); },
            0.5, 0.25, [](double t) { return sin(t + M_PI_4) * (-0.25); },
            0.25, 0.5, [](double t) { return sin(t + M_PI_4) * (0.25) * exp(-2); },
            2, [](double x, double t) { return exp(-x) * sin(M_PI_4 + t); }
        );

//    int x_steps = 1000;
//    int t_steps = 10000;
//    int t_max = 3.0;

//    CrankNicolsonScheme solver;
//    solver.Solve(eq, t_steps, t_max, x_steps, Solver::TWO_POINTS_SECOND_ORDER);

//    double max_error = 0.0;
//    for (int i = 0; i <= x_steps; ++i)
//    {
//        std::cout << std::abs(eq.solution(i * 0.002, t_max) - solver.GetSolution()[t_steps][i]) << std::endl;
//        max_error = std::max(max_error, std::abs(eq.solution(i * 0.002, t_max) - solver.GetSolution()[t_steps][i]));
//    }
//    std::cout << max_error;
    QApplication app(argc, argv);
    MainWindow window(eq);
    window.show();
    exit(app.exec());
}
